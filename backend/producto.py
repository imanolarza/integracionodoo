from flask import Blueprint, render_template

import odoolib
from babel.numbers import format_decimal

from os import environ
producto = Blueprint('producto', __name__, url_prefix='/producto')

# Server odoo constantes
srv = environ.get('url')
db = environ.get('db')
port = int(environ.get('port'))

# Usuarios
user_ttp = environ.get('user_ttp')
pwd_ttp = environ.get('pwd_ttp')

@producto.route('/<id>', methods=['GET'])
def home(id):
    con = odoolib.get_connection(
        hostname=srv,
        database=db,
        login=user_ttp,
        password=pwd_ttp,
        protocol='jsonrpc',
        port=port
    )
    pt = con.get_model('product.template')
    producto = pt.search_read([
            ('id', '=', id)
        ],
        fields=['name', 'product_variant_ids', 'default_code', 'image']
    )
    stock = con.get_model('stock.quant').search_read([
        ('product_id', '=', producto[0]['product_variant_ids']),
        ('location_id.usage', '=', 'internal')
    ])

    total = 0

    for s in stock:
        total += s['quantity']
        s['quantity'] = format_decimal(s['quantity'], locale='es_PY')

    return render_template(
        'producto.html',
        stock=stock,
        total=format_decimal(total, locale='es_PY'),
        producto='[{}] {}'.format(producto[0]['default_code'], producto[0]['name']),
        img='data:image/jpeg;base64,{}'.format(
            producto[0]['image']
        )
    )
