from flask import Flask, request, render_template, redirect, url_for, session, g, jsonify, Response

from flask_cors import CORS
import os
from os import environ
from xmlrpc import client
import odoolib
from babel.numbers import format_decimal
import json
import datetime
import time
import requests

# Constantes
srv = environ.get('url')
db = environ.get('db')
user_ttp = environ.get('user_ttp')
pwd_ttp = environ.get('pwd_ttp')
port = int(environ.get('port'))
ip_scan = environ.get('scanner_ip')

# Iniciar app
app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY'] = 'mysecret'
basedir = os.path.abspath(os.path.dirname(__file__))

# Rutas
@app.route('/api', methods=['GET'])
def home():
    if request.method == 'GET':
        file = open('/cronjson/pedidos.json')

        try:
            res = json.load(file)
            return jsonify(res)
        except json.decoder.JSONDecodeError:
            time.sleep(1)
            res = json.load(file)
            return jsonify(res)

@app.route('/confirmar', methods=['POST'])
def confirmar_pedido():
    global a_confirmar

    if request.method == 'POST':
        data = json.loads(request.data)
        id = int(data['code'][0:-1])

        print(id)

        file = open('/cronjson/pedidos.json')
        dicc = json.load(file)
        ids = [i['id'] for i in dicc['company']['transtape']]
        order_dict = None

        for l in dicc['company']['transtape']:
            if l['id'] == id:
                order_dict = l

        if id in ids:
            con = odoolib.get_connection(
                hostname=srv,
                database=db,
                login=user_ttp,
                password=pwd_ttp,
                protocol='jsonrpc',
                port=port
            )

            orders_model = con.get_model('sale.order')
            order = orders_model.search([('id', '=', id)])

            orders_model.write(order, {'preparado': True})

            return order_dict, 200
        else:
            return  '', 400

@app.route('/status', methods=['POST'])
def status():
    if request.method == 'POST':
        return ''

# Iniciar servidor
if __name__ == '__main__':
    from producto import producto

    # Cargar módulos
    app.register_blueprint(producto)

    app.run(host='0.0.0.0', debug=True)
