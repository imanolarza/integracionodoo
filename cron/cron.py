from xmlrpc import client

from datetime import datetime
from os import environ
import json

db = environ.get('db')
url = environ.get('url')
port = environ.get('port')

user_ttp = environ.get('user_ttp')
user_seg = environ.get('user_seg')
pwd_ttp = environ.get('pwd_ttp')
pwd_seg = environ.get('pwd_seg')

prox = client.ServerProxy('http://%s:%s/xmlrpc/2/object' %  (url, port))
common = client.ServerProxy('http://%s:%s/xmlrpc/2/common' % (url, port))
common.version()

def writeJSON(path, filename, data):
    pathfile = f'{path}/{filename}.json'

    with open(pathfile, 'w') as f:
        json.dump(data, f)

def auth(seg=False):
    user = user_seg if seg else user_ttp
    pwd = pwd_seg if seg else pwd_ttp
    uid = common.authenticate(db, user, pwd, {})

    return uid, pwd

def search_odoo(auth, model, params, options):
    res = prox.execute_kw(
        db, auth[0], auth[1], model, 'search_read', [params], options
    )

    return res

def get_sales(auth, params, options):
    sale_order_ids = search_odoo(
        auth=auth, model='sale.order', params=params, options=options
    )
    sale_order_line_ids = search_odoo(
        auth=auth, model='sale.order.line',
        params=[('order_id', 'in', [so['id'] for so in sale_order_ids])],
        options={'fields': ['name', 'product_uom_qty', 'order_id']}
    )

    for sale_order in sale_order_ids:
        sale_order.update({
            'order_line': [line for line in filter(
                lambda so: so['order_id'][0] == sale_order['id'],
                sale_order_line_ids
            )],
            'date_delivery_order': datetime.strftime(
                datetime.strptime(
                    sale_order['date_delivery_order'],
                    '%Y-%m-%d'
                ),
                '%d/%m/%Y'
            ) if sale_order['date_delivery_order'] else '-'
        })

    return sale_order_ids

if __name__ == '__main__':
    auth_ttp = auth()
    sales_ttp = get_sales(auth=auth_ttp, params=[
        ('state', '!=', 'cancel'),
        ('preparado', '=', False),
        ('date_delivery_order', '>=', datetime.now())
    ], options={
        'fields': [
            'partner_id',
            'name',
            'date_order',
            'order_line',
            'state',
            'company_id',
            'date_delivery_order',
            'comment_pedido',
            'client_order_ref',
        ],
        'limit': 200,
        'order': 'date_delivery_order desc, id desc'
    })

    auth_seg = auth(seg=True)
    sales_seg = get_sales(auth=auth_seg, params=[
        ('state', '=', 'autorizado'),
        ('a_facturar', '=', True)
    ], options={
        'fields': [
            'date_delivery_order',
            'partner_id',
            'name',
            'date_order',
            'order_line',
            'state',
            'company_id'
        ],
    })

    data = {
        'company': {
            'segupak': sales_seg,
            'transtape': sales_ttp,
        },
        'p_cin': len([
            sale for sale in sales_ttp
            if sale['client_order_ref'] == 'Cintas S.A.'
        ]),
        'p_seg': len([
            sale for sale in sales_ttp
            if sale['client_order_ref'] == 'Segupak S.A.'
        ])
    }

    writeJSON(path='/cronjson', filename='pedidos', data=data)

    print('listo')