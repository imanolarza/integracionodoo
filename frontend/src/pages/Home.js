import { React, useEffect, useState } from 'react'

import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import Div from '../components/Div'
import CardPedido from '../components/CardPedido'
import Masonry from '@mui/lab/Masonry'
import Snack from '../components/Snack'

const Home = () => {
    const [pedidos, setPedidos] = useState({})
    const [msg, setMsg] = useState(false)
    useEffect(() => {
        setInterval(() => {
            fetch('/api', {method: 'GET'}).then(response => {
                if (response.ok) {
                    console.log(response)
                    return response.json()
                }
                throw response
            }).then(data => {
                setPedidos(data)
                setMsg(false)
            }).catch(e => {
                setMsg('Sin conexión, intentando reconectar')
            })
        }, 1500)
    }, []);
    return (<>
        <Box padding={1}>
            <Snack msg={msg} setMsg={setMsg} severity="error"/>
            <Grid container spacing={1}>
                <Div item xs={2} bgColor='#008ECC'>
                    <Typography variant="h6" gutterBottom>
                        Segupak <Chip label={pedidos.company ? pedidos.company.segupak.length: 0} style={{backgroundColor: '#6200EE'}}/>
                    </Typography>
                    <Masonry columns={1}>{
                        pedidos.company !== undefined ?
                        pedidos.company.segupak.map(pedido => {
                        return(<>
                            <CardPedido
                                bgColor='#6200EE'
                                partnerId={pedido.partner_id[1]}
                                soName={pedido.name}
                                orderLine={pedido.order_line}
                                comment={pedido.comment_pedido}
                                dateDelivery={pedido.date_delivery_order}
                                companyId={pedido.company_id[0]}/>
                        </>)
                        }) : undefined
                    }</Masonry>
                </Div>
                <Div item xs={10} bgColor='#222222'>
                    <Grid container>
                        <Grid item xs={4}>
                            <Typography variant="h6" gutterBottom>
                                Cantidad de pedidos <Chip label={pedidos.p_cin + pedidos.p_seg}/>
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="h6" gutterBottom>
                                Pedidos Cintas <Chip label={pedidos.p_cin} style={{backgroundColor: '#1565c0'}}/>
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="h6" gutterBottom>
                                Pedidos Segupak <Chip label={pedidos.p_seg} style={{backgroundColor: '#6200EE'}}/>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Masonry columns={4} spacing={1}>{
                        pedidos.company !== undefined ?
                        pedidos.company.transtape.map(pedido => {
                        return(<>
                            <CardPedido
                                bgColor={
                                    pedido.client_order_ref === 'Segupak S.A.' ?
                                    '#6200EE': '#1565c0'
                                }
                                partnerId={pedido.partner_id[1]}
                                soName={pedido.name}
                                orderLine={pedido.order_line}
                                comment={pedido.comment_pedido}
                                dateDelivery={pedido.date_delivery_order}
                                companyId={pedido.company_id[0]}/>
                        </>)
                        }) : undefined
                    }</Masonry>
                </Div>
            </Grid>
        </Box>
    </>)
}

export default Home
