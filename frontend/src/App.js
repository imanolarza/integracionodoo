import './App.css'
import React from 'react'
import { ThemeProvider } from '@mui/system'
import { createTheme, CssBaseline } from '@mui/material'
import { deepPurple } from '@mui/material/colors'
import Home from './pages/Home'

const darkTheme = createTheme({
    palette: {
      mode: 'dark',
      primary: {
          main: deepPurple[300]
      }
    },
  })
function App() {
    return (
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <Home />
        </ThemeProvider>
    )
}

export default App
