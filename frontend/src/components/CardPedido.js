import React from 'react'

import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import ChipPedido from './ChipPedido'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import Box from '@mui/material/Box'
import NumberFormat from "react-number-format"

const CardPedido = (props) => {
    const bgColor = props.bgColor
    const partnerId = props.partnerId
    const soName = props.soName
    const orderLine = props.orderLine
    const comment = props.comment
    const dateDelivery = props.dateDelivery
    const companyId = props.companyId

    return (<Card style={{backgroundColor: bgColor}}>
        <CardContent>
            <Grid container component={Box} paddingBottom={1}>
                <Typography variant="h5" fontWeight="bold">
                    {partnerId}
                </Typography> &nbsp; <ChipPedido label={soName}/>
            </Grid>
            <Divider />
            <List>
                {orderLine.map(line => {
                    return (<>
                        <ListItem>
                            <Grid
                                container
                                direction="row"
                                justifyContent="space-between"
                                alignItems="center">
                                <Grid item xs={8}>{line.name}</Grid>
                                <Grid item xs={4}>
                                    <ChipPedido label={
                                        <NumberFormat
                                            value={line.product_uom_qty}
                                            displayType="text"
                                            thousandSeparator="."
                                            decimalSeparator=","
                                            decimalScale={2}/>
                                    }/>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <Divider />
                    </>)
                })}
            </List>
            {companyId === 4 ?
            <List>
                <ListItem>
                    <Typography variant="h6" style={{color: '#e53935'}}>
                        Observación: {comment}
                    </Typography>
                </ListItem>
                <ListItem component={Typography} variant="h6">
                    Fecha de entrega: {dateDelivery}
                </ListItem>
            </List>: undefined}
        </CardContent>
    </Card>)
}

export default CardPedido
