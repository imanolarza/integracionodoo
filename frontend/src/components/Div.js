import React from 'react'

import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import Paper from '@mui/material/Paper'
import Box from '@mui/material/Box'


const Div = (props) => {return (
    <Grid item { ...props }>
        <Paper component={Box} padding={1} style={{backgroundColor: props.bgColor}}>
            <Typography variant="h6" gutterBottom>{props.text}</Typography>
            <Grid container>{props.children}</Grid>
        </Paper>
    </Grid>
)}

export default Div
