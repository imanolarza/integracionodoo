import React from 'react'

import Chip from '@mui/material/Chip'

const ChipPedido = (props) => {
    return(
        <Chip
            style={{
                backgroundColor: '#ffffff',
                color: '#000000',
                fontWeight: 'bold',
            }} { ...props }/>
    )
}

export default ChipPedido
